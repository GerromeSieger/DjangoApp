from django.contrib import admin
from .models import Epl, Laliga, SerieA, League

admin.site.register(Epl)
admin.site.register(SerieA)
admin.site.register(Laliga)
admin.site.register(League)
# Register your models here.
