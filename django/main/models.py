from django.db import models
from django.contrib.auth.models import User

class League(models.Model):
    le = models.CharField(max_length=30)
    epl = models.ImageField(upload_to='images/', null=True)
    laliga = models.ImageField(upload_to='images/', null=True)
    ligue1 = models.ImageField(upload_to='images/', null=True)
    seriea = models.ImageField(upload_to='images/', null=True)
    bundes = models.ImageField(upload_to='images/', null=True)
    portleague = models.ImageField(upload_to='images/', null=True)

    def __str__(self):
        return self.le

class Epl(models.Model):
    team = models.CharField(max_length=20)
    description = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.team

class Laliga(models.Model):
    team = models.CharField(max_length=20)
    description = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.team

class SerieA(models.Model):
    team = models.CharField(max_length=20)
    description = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.team



