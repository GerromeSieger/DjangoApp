from django.urls import path, include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path("", views.home, name="home"),
    path("register/", views.register, name="register"),
    path("about/", views.about, name="about"),
    path("contact/", views.contact, name="contact"),
    path("news/", views.news, name="news"),
    path("footballers/", views.footballers, name="footballers"),
    path("post/", views.post, name="post"),
    path("leagues/", views.leagues, name="leagues"),
    path("singlepost/", views.singlepost, name="singlepost"),
    path("", include("django.contrib.auth.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)